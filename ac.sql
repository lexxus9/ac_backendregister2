-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 04, 2018 at 07:02 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ac`
--

-- --------------------------------------------------------

--
-- Table structure for table `register2`
--

CREATE TABLE `register2` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `cnp` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `birth` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `question` varchar(255) NOT NULL,
  `facultate` varchar(30) DEFAULT NULL,
  `sex` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `register2`
--

INSERT INTO `register2` (`id`, `firstname`, `lastname`, `phone`, `email`, `cnp`, `facebook`, `birth`, `department`, `question`, `facultate`, `sex`) VALUES
(38, 'asdadas', 'asdadada', '0747874727', 'asdada23221@gmail.com', '3980831170043', 'https://www.facebook.com/alex.dragomir04', '1999-12-12', 'it', 'aosdkdal;[dkadas', 'asdad', 'M'),
(39, 'asdadas', 'asdadada', '0747874727', 'asdada232212@gmail.com', '3980831170043', 'https://www.facebook.com/alex.dragomir04', '1999-12-12', 'it', 'aosdkdal;[dkadas', 'asdad', 'M'),
(40, 'asdadas', 'asdadada', '0747874727', 'asdada2322312@gmail.com', '3980831170043', 'https://www.facebook.com/alex.dragomir04', '1999-12-12', 'it', 'aosdkdal;[dkadas', 'asdad', 'M'),
(41, 'asdadas', 'asdadada', '0747874727', 'asdada4312@gmail.com', '3980831170043', 'https://www.facebook.com/alex.dragomir04', '1999-12-12', 'it', 'aosdkdal;[dkadas', 'asdad', 'M'),
(42, 'asdadas', 'asdadada', '0747874727', 'asdada52@gmail.com', '3980831170043', 'https://www.facebook.com/alex.dragomir04', '1999-12-12', 'it', 'aosdkdal;[dkadas', 'asdad', 'M'),
(43, 'asdadas', 'asdadada', '0747874727', 'asdada512@gmail.com', '3980831170043', 'https://www.facebook.com/alex.dragomir04', '1999-12-12', 'it', 'aosdkdal;[dkadas', 'asdad', 'M'),
(44, 'asdadas', 'asdadada', '0747874727', 'asdada2512@gmail.com', '3980831170043', 'https://www.facebook.com/alex.dragomir04', '1999-12-12', 'it', 'aosdkdal;[dkadas', 'asdad', 'M'),
(45, 'asdadas', 'asdadada', '0747874727', 'asdada25312@gmail.com', '3980831170043', 'https://www.facebook.com/alex.dragomir04', '1999-12-12', 'it', 'aosdkdal;[dkadas', 'asdad', 'M'),
(46, 'asdadas', 'asdadada', '0747874727', 'asdada215312@gmail.com', '3980831170043', 'https://www.facebook.com/alex.dragomir04', '1999-12-12', 'it', 'aosdkdal;[dkadas', 'asdad', 'M'),
(47, 'asdadas', 'asdadada', '0747874727', 'asdada2123115312@gmail.com', '3980831170043', 'https://www.facebook.com/alex.dragomir04', '1999-12-12', 'it', 'aosdkdal;[dkadas', 'asdad', 'M'),
(48, 'asdadas', 'asdadada', '0747874727', 'asdada21223115312@gmail.com', '3980831170043', 'https://www.facebook.com/alex.dragomir04', '1999-12-12', 'it', 'aosdkdal;[dkadas', 'asdad', 'M'),
(49, 'asdadas', 'asdadada', '0747874727', 'asdada2112@gmail.com', '3980831170043', 'https://www.facebook.com/alex.dragomir04', '1999-12-12', 'it', 'aosdkdal;[dkadas', 'asdad', 'M'),
(50, 'asdadas', 'asdadada', '0747874727', 'asdada2132@gmail.com', '3980831170043', 'https://www.facebook.com/alex.dragomir04', '1999-12-12', 'it', 'aosdkdal;[dkadas', 'asdad', 'M'),
(51, 'asdadas', 'asdadada', '0747874727', 'asdada21342@gmail.com', '3980831170043', 'https://www.facebook.com/alex.dragomir04', '1999-12-12', 'it', 'aosdkdal;[dkadas', 'asdad', 'M'),
(52, 'asdadas', 'asdadada', '0747874727', 'asdada21352@gmail.com', '3980831170043', 'https://www.facebook.com/alex.dragomir04', '1999-12-12', 'it', 'aosdkdal;[dkadas', 'asdad', 'M'),
(53, 'asdadas', 'asdadada', '0747874727', 'asdada211352@gmail.com', '3980831170043', 'https://www.facebook.com/alex.dragomir04', '1999-12-12', 'it', 'aosdkdal;[dkadas', 'asdad', 'M'),
(54, 'asdadas', 'asdadada', '0747874727', 'asdada2115352@gmail.com', '3980831170043', 'https://www.facebook.com/alex.dragomir04', '1999-12-12', 'it', 'aosdkdal;[dkadas', 'asdad', 'M'),
(55, 'asdadas', 'asdadada', '0747874727', 'asdada21115352@gmail.com', '3980831170043', 'https://www.facebook.com/alex.dragomir04', '1999-12-12', 'it', 'aosdkdal;[dkadas', 'asdad', 'M'),
(56, 'asdadas', 'asdadada', '0747874727', 'asdada211155352@gmail.com', '3980831170043', 'https://www.facebook.com/alex.dragomir04', '1999-12-12', 'it', 'aosdkdal;[dkadas', 'asdad', 'M'),
(57, 'asdadas', 'asdadada', '0747874727', 'asdada6@gmail.com', '3980831170043', 'https://www.facebook.com/alex.dragomir04', '1999-12-12', 'it', 'aosdkdal;[dkadas', 'asdad', 'M'),
(58, 'asdadas', 'asdadada', '0747874727', 'asdada116@gmail.com', '3980831170043', 'https://www.facebook.com/alex.dragomir04', '1999-12-12', 'it', 'aosdkdal;[dkadas', 'asdad', 'M'),
(59, 'asdadas', 'asdadada', '0747874727', 'asdada1116@gmail.com', '3980831170043', 'https://www.facebook.com/alex.dragomir04', '1999-12-12', 'it', 'aosdkdal;[dkadas', 'asdad', 'M'),
(60, 'asdadas', 'asdadada', '0747874727', 'asdada11221316@gmail.com', '2980831170043', 'https://www.facebook.com/alex.dragomir04', '1999-12-12', 'it', 'aosdkdal;[dkadas', 'asdad', 'F'),
(61, 'asdadas', 'asdadada', '0747874727', 'asdada112231316@gmail.com', '2980831170043', 'https://www.facebook.com/alex.dragomir04', '1999-12-12', 'it', 'aosdkdal;[dkadas', 'asdad', 'F'),
(62, 'asdadas', 'asdadada', '0747874727', 'asdada1122311316@gmail.com', '2980831170043', 'https://www.facebook.com/alex.dragomir04', '1999-12-12', 'it', 'aosdkdal;[dkadas', 'asdad', 'F'),
(63, 'asdadas', 'asdadada', '0747874727', 'asdada11231122311316@gmail.com', '2980831170043', 'https://www.facebook.com/alex.dragomir04', '1999-12-12', 'it', 'aosdkdal;[dkadas', 'asdad', 'F'),
(64, 'asdadas', 'asdadada', '0747874727', 'asdada1121231122311316@gmail.com', '2980831170043', 'https://www.facebook.com/alex.dragomir04', '1999-12-12', 'it', 'aosdkdal;[dkadas', 'asdad', 'F'),
(65, 'asdadas', 'asdadada', '0747874727', 'asdad2311316@gmail.com', '2980831170043', 'https://www.facebook.com/alex.dragomir04', '1999-12-12', 'it', 'aosdkdal;[dkadas', 'asdad', 'F'),
(66, 'asdadas', 'asdadada', '0747874727', 'asdad231211316@gmail.com', '1990204170082', 'https://www.facebook.com/alex.dragomir04', '1999-02-04', 'it', 'aosdkdal;[dkadas', 'asdad', 'M'),
(67, 'asdadas', 'asdadada', '0747874727', 'asdad23121231311316@gmail.com', '1990304170082', 'https://www.facebook.com/alex.dragomir04', '1999-02-04', 'it', 'aosdkdal;[dkadas', 'asdad', 'M'),
(68, 'asdadas', 'asdadada', '0747874727', 'asdad2321121231311316@gmail.com', '1990304170082', 'https://www.facebook.com/alex.dragomir04', '1999-03-04', 'it', 'aosdkdal;[dkadas', 'asdad', 'M'),
(70, 'asdadas', 'asdadada', '0747874727', 'asdad23221121231311316@gmail.com', '1990304170082', 'https://www.facebook.com/alex.dragomir04', '1999-03-04', 'it', 'aosdkdal;[dkadas', 'asdad', 'M');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `register2`
--
ALTER TABLE `register2`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_uq` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `register2`
--
ALTER TABLE `register2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
