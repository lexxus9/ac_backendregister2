<?php

require_once("dbconfig.php");

$firstname = "";
$lastname = "";
$phone = "";
$email = "";
$cnp = "";
$facebook = "";
$birth = "";
$department = "";
$question = "";
$captcha_generated = "";
$captcha_inserted = "";
$check = "";
$error=0;
$error_text="";
$facultate="";
$sex="";

if(isset($_POST['firstname'])){
	$firstname = $_POST['firstname'];
}

if(isset($_POST['lastname'])){
	$lastname = $_POST['lastname'];
}

if(isset($_POST['phone'])){
	$phone = $_POST['phone'];
}

if(isset($_POST['email'])){
	$email = $_POST['email'];
}

if(isset($_POST['cnp'])){
	$cnp = $_POST['cnp'];
}

if(isset($_POST['facebook'])){
	$facebook = $_POST['facebook'];
}

if(isset($_POST['birth'])){
	$birth = $_POST['birth'];
}

if(isset($_POST['department'])){
	$department = $_POST['department'];
}

if(isset($_POST['question'])){
	$question = $_POST['question'];
}

if(isset($_POST['captcha_generated'])){
	$captcha_generated = $_POST['captcha_generated'];
}

if(isset($_POST['captcha_inserted'])){
	$captcha_inserted = $_POST['captcha_inserted'];
}

if(isset($_POST['check'])){
	$check = $_POST['check'];
}

if(isset($_POST['facultate'])){
	$facultate= $_POST['facultate'];
}

$age = date_diff(date_create($birth), date_create('now'))->y;

if((strlen($firstname)<3 || strlen($firstname)>20)||(!preg_match ('/^([a-zA-Z]+)$/', $firstname)))
{
	$error=1;
	$error_text="Firstname has less than 3 characters or more than 20 or it contains a number";
}
if((strlen($lastname)<3 || strlen($lastname)>20)||(!preg_match ('/^([a-zA-Z]+)$/', $lastname)))
{
	$error=1;
	$error_text="lastname has less than 3 characters or more than 20 or it contains a number";
}

if(strlen($question) <15){
	$error=1;
	$error_text="Question has less than 15 ch";
}

if(!is_numeric($phone)||strlen($phone)!=10){
	$error=1;
	$error_text="Phone number has a letter or it is smaller than 10";
}

if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
	$error=1;
	$error_text='Invalid email format';
}
 if(empty($firstname) || empty($lastname) || empty($phone)||empty($facebook) || empty($birth)|| empty($captcha_inserted)|| empty($check) || empty($question) || empty($facultate))
  {
  	$error=1;
  	$error_text='One or more fields are empty';}

if(strlen($cnp)!=13)
{
	$error=1;
	$error_text="cnp invalid";
}
if((substr($cnp,-13,1)!='1')&&(substr($cnp,-13,1)!='2')&&(substr($cnp,-13,1)!='3')&&(substr($cnp,-13,1)!='4')&&(substr($cnp,-13,1)!='5')&&(substr($cnp,-13,1)!='6'))
{
	$error=1;
	$error_text="prima cifra nu e buna";
}
if(strpos($facebook,'www.facebook.com')==false){
	$error=1;
	$error_text="Link de fb invalid";
}

if(($age<18) || ($age>100)){
	$error=1;
	$error_text="Varsta mai mica decat 18 ani sau mai mare decat 100";
}

if($captcha_inserted!=$captcha_generated){
	$error=1;
	$error_text="Captcha gresit";
}

if((strlen($facultate)<3 || strlen($facultate)>30)||(!preg_match ('/^([a-zA-Z]+)$/', $facultate)))
{
	$error=1;
	$error_text="Facultate contine o cifra sau nu are dimensiunea potrivita(3-30)";
}

$c=substr($cnp,-13,1);
 if($c%2 == 0) $sex="F";
else  $sex="M";

if((substr($birth,-8,2)!=substr($cnp,-12,2))||(substr($birth,-5,2)!=substr($cnp,-10,2))||(substr($birth,-2,2)!=substr($cnp,-8,2)))
{ echo "Cnp-ul introdus nu e valabil";
 $error=1;}

//mi-am permis sa schimb pdo in mysqli

  $connection_mysql = mysqli_connect(HOST,USER,PASSWORD,DATABASE);
	if (mysqli_connect_errno($connection_mysql))
 {
 		echo "Failed to connect to MySQL: " . mysqli_connect_error();
		$error=1;
  }

	if($email != "") {
		$sql1= "select * from register2 where email='$email'";
		if ($result = mysqli_query($connection_mysql,$sql1))
		      $rowcount = mysqli_num_rows($result);
	if($rowcount>0)
		{
			$error=1;
			echo "Emailul deja exista";
		}}

		$id="select id from register2";
		if ($resul1 = mysqli_query($connection_mysql,$id))
					$idcount = mysqli_num_rows($resul1);
				if($idcount>50){
					$error=1;
					echo "Limita participantilor a fost atinsa, ne pare rau!";
				}




if ($error==0){
$sql="INSERT INTO register2 (firstname,lastname,phone,email,cnp,facebook,birth,department,question,facultate,sex) VALUES('$firstname','$lastname','$phone','$email','$cnp','$facebook','$birth','$department','$question','$facultate','$sex')";

if ($connection_mysql->query($sql) === TRUE) {
		echo "Multumim ca te-ai inregistrat!";}
		else {
		    echo "Error: " . $sql . "<br>" . $connection_mysql->error;
		}

}
else{ echo $error_text;
}
mysqli_close($connection_mysql);
